/**********************************************************************
 * wordstats.cpp - CSCI251 - Ass4 - Contains WordsStats class definition
 * <your name> <your login> <date last modified>
 **********************************************************************/
#include <iostream>
#include <iomanip>
#include <fstream>
#include <string>
#include <cstring>
#include <cstdlib>
#include <algorithm> // for transform()
#include "wordstats.h"
using namespace std;

WordStats::WordStats(){
	strcpy(Filename,"");
}

// Reads dictionary.txt into Dictionary
void WordStats::ReadDictionary(){
    ifstream fin;
    string tmpWord;

    fin.open("dictionary.txt");
    if ( !fin.good() )
    {
        cerr << "Could not open dictionary file!" << endl;
        exit(1);
    }

    while(fin >> tmpWord){        
        Dictionary.insert(tmpWord);
    }

    cout << Dictionary.size() << " words are in dictionary" << endl;
    fin.close();

}

// Reads dictionary.txt into Dictionary
void WordStats::AddToDictionary(){
    string newWord;
    cout << "Enter word to add to dictionary: ";
    cin >> newWord;
    cin.ignore();

    NormalizeWord(newWord); // turn it into lowercase and remove punctuations
    if(Dictionary.find(newWord) != Dictionary.end()) {
        // already in dictionary
        cout << "Word is already in dictionary." << endl;
        return;
    }

    ofstream fout;
    fout.open("dictionary.txt", ios_base::app);

    if(!fout){
        cout << "Cant find dictionary file!" << endl;
        exit(0);
    }

    fout << newWord << endl;
    Dictionary.insert(newWord);
    fout.close();
    cout << "Word added to dictionary" << endl;

}

// Displays Dictwords 10 at a time
void WordStats::DisplayDictionary(){
    set<string>::iterator it;
    int i = 0;
    for (it = Dictionary.begin(); it != Dictionary.end(); ++it) {
        cout << *it << endl;
        ++i;

        if(i%10 == 0){
            cout<<"Continue? (y/n):";
            char Ans;
            cin>>Ans;
            cout<<endl;
            if(Ans=='n') return;
        }
    }
}

// Reads textfile into KnownWords and UnknownWords
void WordStats::ReadTxtFile(){
    cout << "Enter filename: ";
    cin >> Filename;
    cin.ignore();

    ifstream fin;
    string tmpWord;
    fin.open(Filename);

    if ( !fin.good() )
    {
        cerr << "Could not open dictionary file!" << endl;
        exit(1);
    }

    int position = 0;
    while(fin >> tmpWord) {
        // Normalize tmpWord
        NormalizeWord(tmpWord);
        if(tmpWord.size() == 0 || isdigit(tmpWord[0])) {
            continue;
        }
        if(Dictionary.find(tmpWord) != Dictionary.end()) {
            // Is in known words
            KnownWords[tmpWord].push_back(position);
        }
        else {
            UnknownWords[tmpWord].push_back(position);
        }
        position++;

    }
    cout << KnownWords.size() << " known words read." << endl;
    cout << UnknownWords.size() << " unknown words read." << endl;
}

// Displays stats of words in KnownWords
void WordStats::DisplayKnownWordStats(){
    DisplayWordStats(KnownWords);   
}

// Displays stats of words in Unknownwords
void WordStats::DisplayUnknownWordStats(){
    DisplayWordStats(UnknownWords);
}

// Displays 20 most frequent words in KnownWords
void WordStats::DisplayMostFreqKnownWords(){
    DisplayMostFreqWords(KnownWords);
}

// Displays 20 most frequent words in UnknownWords
void WordStats::DisplayMostFreqUnknownWords(){
    DisplayMostFreqWords(UnknownWords);
}

// Displays original text from KnownWords & UnknownWords
void WordStats::DisplayOriginalText(){
    multimap<int,string> mm;

    for (WordMapIter it = KnownWords.begin(); it != KnownWords.end(); ++it){        
        for (vector<int>::iterator i = it->second.begin(); i != it->second.end(); ++i)
        {
            mm.insert(make_pair(*i, it->first));
        }
    }

    for (WordMapIter it = UnknownWords.begin(); it != UnknownWords.end(); ++it){        
        for (vector<int>::iterator i = it->second.begin(); i != it->second.end(); ++i)
        {
            mm.insert(make_pair(*i, it->first));
        }
    }

    cout << "--Original Text--"<< endl;
    int i = 0;
    for (multimap<int,string>::iterator it = mm.begin(); it != mm.end(); ++it){    
        cout << it->second << " ";
    }
}

// Normalizes word. Converts it into lowercase and removes
// punctuation marks
void WordStats::NormalizeWord(string &word){
    for(int i = 0; i < word.size(); ++i) {
        if(i == word.size() - 1 && ispunct(word[i])) {
            word.resize(i);
            break;
        }
        word[i] = tolower(word[i]);
    }
}

void WordStats::DisplayWordStats(WordMap &WMap) {
    cout << setw(20) << "Word" 
         << setw(10) << "Count" 
         << setw(14) << "Position(s)" << endl;
    for (WordMapIter it = WMap.begin(); it != WMap.end(); ++it){
        cout << setw(20) << it->first // key
         << setw(10) << it->second.size()  // list of positions
         << "   ";

        for (vector<int>::iterator i = it->second.begin(); i != it->second.end(); ++i)
        {
            cout << *i << " "; 
        }
        cout << endl;
        
    }
}


void WordStats::DisplayMostFreqWords(WordMap &WMap){
    multimap<int,string> mm;

    for (WordMapIter it = WMap.begin(); it != WMap.end(); ++it){        
        mm.insert(make_pair(it->second.size(), it->first));
    }

    cout << setw(20) << "Word" << setw(10) << "Count" << endl;
    int i = 0;
    for (multimap<int,string>::reverse_iterator it = mm.rbegin(); it != mm.rend(); ++it){    
        cout << setw(20) << it->second << setw(10) << it->first << endl; 
        ++i;
        if (i == 10) {
            break;
        }
    }
}

